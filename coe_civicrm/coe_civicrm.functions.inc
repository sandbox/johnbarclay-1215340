<?php

function _coe_civicrm_user_set_groups(&$account) {
  if (! $civicrm_user_data = _coe_civicrm_get_civicrm_data($account->uid, TRUE)) return;
  $civicrm_contact_id = (int)$civicrm_user_data['contact_id'];
  $data['civicrm_groups'] =  (count($civicrm_user_data['groups']) > 0) ? $civicrm_user_data['groups'] : NULL;
  $account = user_save($account, $data, 'civicrm');
  // dpm("_coe_civicrm_user_set_groups: \$account after user_save call"); dpm($account);
 }
 
 function _coe_civicrm_get_all_groups($reset = FALSE) {
  static $groups;
  
  if ($reset || !isset($groups) ) {
   $groups = array();
   civicrm_initialize(true);
   require_once './sites/all/modules/civicrm/api/v2/Group.php';
   $groups = @civicrm_group_get();
   // dpm("_coe_civicrm_get_all_groups: \$groups"); dpm($groups);

  }


	return $groups;
 }
	 
   
function _coe_civicrm_get_civicrm_data($uid = NULL, $reset = FALSE) {

  static $civicrm_user_data;
  
  if (! isset($civicrm_user_data) || $reset) {
    civicrm_initialize(true);
    require_once './sites/all/modules/civicrm/api/v2/Contact.php';
    require_once './sites/all/modules/civicrm/api/v2/utils.php';
    require_once './sites/all/modules/civicrm/api/v2/Group.php';
    require_once './sites/all/modules/civicrm/api/v2/GroupContact.php';
    require_once './sites/all/modules/civicrm/api/UFGroup.php';
  
    if (! $uid) {
      global $user;
      $uid = $user->uid;
    }
    $contact_id = crm_uf_get_match_id($uid);
    $params  = array('contact_id' => $contact_id);
    $civicrm_user_object = &civicrm_contact_get( $params );
   
    if (is_array($civicrm_user_object)) {
       $civicrm_user_data = $civicrm_user_object[$contact_id];
      $civicrm_groups = civicrm_group_contact_get($params);
       foreach($civicrm_groups as $id => $data) {
        $civicrm_user_data['groups'][$data['group_id']] = $data;
        
       }
    } 
  }
  
  // dpm("_coe_civicrm_get_civicrm_data \$civicrm_user_data");  dpm($civicrm_user_data);
  return $civicrm_user_data;
  
}

