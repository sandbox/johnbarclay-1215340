<?php

define('COE_CIVICRM_LOAD_GROUPS_DEFAULT', 1);

function coe_civicrm_configure(&$form_state) {
 $form['#title'] = "Configure Setttings and Constants related to civicrm.";
 $form['#prefix'] = ""; 
 _coe_civicrm_admin_form($form);
  return system_settings_form($form);
}

function _coe_civicrm_admin_form(&$form) {
 
   $form['coe_civicrm_configure']['coe_civicrm_load_groups'] = array(
    '#type' => 'checkbox',
    '#required' => FALSE,
    '#title' => t('Load civicrm groups into user data when user logs in.  Utilized by Awards module.'),
    '#default_value' => variable_get('coe_civicrm_load_groups', COE_CIVICRM_LOAD_GROUPS_DEFAULT), 
    );
 
}

