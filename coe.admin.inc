<?php

/**
 * note that these constants are generally only available in this form; not the entire modue.
 * on install they are
 * propagated to the drupal variables table by the hook_install function for this module
 **/

define('COE_UIUC_NAME', 'University of Illinois');
define('COE_HTTPS_AUTH_DEFAULT', 1);

function coe_configure(&$form_state) {

 $form['#title'] = "Configure COE Overall Setttings and Constants";
 $form['#prefix'] = "just a placeholder for general configurable preferences"; 
  _coe_admin_form($form);
  return system_settings_form($form);
}

function _coe_admin_form(&$form) {
 
   $form['coe_configure']['coe_uiuc_name'] = array(
    '#type' => 'textfield',
    '#size' => 50,
    '#required' => FALSE,
    '#title' => t('Current Branding Name for UIUC'),
    '#description' => "eg ". COE_UIUC_NAME . " (this is just a dummy placeholder for the module)",
    '#default_value' => variable_get('coe_uiuc_name', COE_UIUC_NAME), 
    );

}

