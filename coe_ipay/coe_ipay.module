<?php

/**
 * @file
 * Module with University of Illinois iPay (ipay) API and supporting drupal content types
 *  for ipay site definitions, products, and transactions.
 */


define('COE_IPAY_STORES_CACHE_ID', 'coe_ipay_stores');
define('COE_IPAY_STORE_TAGS_CACHE_ID', 'coe_ipay_store_tags');
define('COE_IPAY_CC_STMT_MSG', 'U of IL Online Payment.');
define('COE_IPAY_CURL_TESTED_VER', '7.15');
define('COE_IPAY_DEBUG_FUNCTION', 'dpm');
define('IPAY_SITE_BAD_TOKEN',"
	Bad iPay Token.<br/>
	Perhaps this transaction has already been completed?
	Please contact the site administrator to resolve this.<br/>
");



/**
 * Implementation of hook_perm().
 */
function coe_ipay_perm() {
	return array('use coe ipay','administer coe ipay');
}



/**
 * Implementation of hook_menu().
 */
function coe_ipay_menu() {

	/**
		*  Initiates iPay Transaction and redirects to iPay.
		*  return page after ipay site will be Response URL
		*  eg: ipay/passthrough/456/1x2,2x83  for 1 of product 2, 2 of product 83 on site # 456
	 **/
	
   global $slippers_dev;
	$items['ipay/passthrough/%'] = array(
		'title' => 'COE iPay Passthrough Transaction Start Page',
			'page callback' => 'coe_ipay_passthrough_start',
			'page arguments' => array(2, 3, $slippers_dev),
			'access callback' => 'user_access',
			'access arguments' => array('use coe ipay'),
			'file' => 'coe_ipay_drupal_functions.inc',
		);
	
	/** single return url exists for all ipay sites.
	 * transaction data will determine which coe ipay site id
	 * is relevant
	 **/
	
  $items['ipay/response'] = array(
		'title' => 'iPay Response Page',
		'page callback' => 'coe_ipay_receipt',
		'page arguments' => NULL,
		'access callback' => 'user_access',
		'access arguments' => array('use coe ipay'),
		'file' => 'coe_ipay_drupal_functions.inc',
		'type' => MENU_CALLBACK,
		);
	
  $items['test/ipay/response'] = array(
		'title' => 'iPay Response Page',
		'page callback' => 'coe_ipay_receipt',
		'page arguments' => NULL,
		'access callback' => 'user_access',
		'access arguments' => array('use coe ipay'),
		'file' => 'coe_ipay_drupal_functions.inc',
		'type' => MENU_CALLBACK,
		);
       
  
	
	/* admin and testing pages */
	$items['admin/settings/ipay'] = array(
		'title' => 'COE iPay Settings',
		'description' => 'Configure COE iPay functionality and constants.',
		'page callback' => 'drupal_get_form',
		'page arguments' => array('coe_ipay_configure'),
		'access callback' => 'user_access',
		'access arguments' => array('administer coe ipay'),
		'file' => 'coe_ipay.admin.inc',
		'type' => MENU_LOCAL_TASK,
	);
	
  return $items;
}

/**
 * Implementation of hook_form_alter().
 */
function coe_ipay_form_alter(&$form, &$form_state, $form_id) {

  if (isset($form['type']) && isset($form['#node'])) {
		switch ($form['#node']->type) {
			
			case 'ipay_store':
				$token_legend = "The following tokens may be used in the template." .  theme('token_help','coe_ipay');
				$form['field_ipay_site_success_tpl'][0]['#description'] =  $token_legend;
				$form['field_ipay_site_fail_tpl'][0]['#description'] =  $token_legend;
				$form['field_ipay_site_cancel_tpl'][0]['#description'] =  $token_legend;
				
				$form['#validate'][] = "coe_ipay_store_conf_form_validate";
			break;
		
			case 'ipay_prd':
				$form['#validate'][] = "coe_ipay_product_form_validate";
			break;
		}
  }
}


/**
 * Implementation of hook_token_list().
 */

function coe_ipay_token_list($type = NULL) {  // content related to a site and trans.
	$tokens = array();
	if ($type == 'coe_ipay') {
		$tokens['coe_ipay']['capture_amount'] = t("Total iPay captured amount");
		$tokens['coe_ipay']['transaction_id'] = t("iPay tranasction id");
		$tokens['coe_ipay']['timestamp'] = t("timestamp of transaction");
		$tokens['coe_ipay']['drupal_trans_nid'] = t("Drupal transaction node id.");
		$tokens['coe_ipay']['response_code'] = t("iPay response code.  Will be 0 for success.");
		$tokens['coe_ipay']['response_code_text'] = t("iPay response code text.  Empty for success.");
		$tokens['coe_ipay']['response_code_state'] = t("Success or Failure");
		$tokens['coe_ipay']['ipay_credit_card_stmt'] = t("Text appearing on credit card statement.");
		$tokens['coe_ipay']['products_list'] = t("themed table of products.");
		}
	return $tokens;
}

/**
 * Implementation of hook_token_values().
 */

function coe_ipay_token_values($type = NULL, $object = NULL, $options = array()) {
	$tokens = array();
 	if ($type == 'coe_ipay' && is_array($object)) {
		foreach ($object as $token => $value) {
			$tokens[$token] = $value;
		}
 	}
	return $tokens;
} 

/**
 * Implementation of hook_node_api().
 */

function coe_ipay_node_api(&$node, $op) {
	//need to clear out site cache when products or site nodes change.
	

	if ($node->type == 'ipay_store' && ($op == 'delete' || $op == 'update')) {
		if ($site_nid = $node->nid) {
			require_once('coe_ipay_drupal_functions.inc');
			$sites = coe_ipay_stores(TRUE);  // flush site cache
		}
	}
	
	if ($node->type == 'ipay_prd' && ($op == 'insert' || $op == 'delete' || $op == 'update')) {
		if ($site_nid = $node->field_ipay_prdt_site_id[0]['nid']) {
			require_once('coe_ipay_drupal_functions.inc');
			$sites = coe_ipay_stores(TRUE);  // flush site cache
		}
	}
	
}


/*

  hook_coe_ipay_trans_registered_data
 
  invoked at start of transaction after register with ipay.
  give opportunity to add data to transaction.data field.
  use cases might uid, civicrm event id, etc.
  
  @param int $trans_nid.  cck nid of transaction record.
  @param node $trans. trans node.
  @param array data.  Data stored in transaction data field
  @param string fkname.  optional foreign key name transaction
  @param string fkvalue.  option foreign key value for transaction.
  returns array data by reference.
  
*/

function hook_coe_ipay_trans_registered_data($trans_nid, $trans, &$data, &$fk_name, &$fk_value) {
	

}

/*
  hook_coe_ipay_finished_data
 
  invoked at end of transaction  regardless of error, cancel, or success.
  give opportunity to add data to transaction.data field.
  use cases might uid, civicrm event id, etc.
  
  @param array response.  ipay response data.
  @param int $trans_nid.  cck nid of transaction record.
  @param node $trans. trans node.
  @param array data.  Data stored in transaction data field
  returns array data by reference.
  
*/

function hook_coe_ipay_finished_data($response, $trans_nid, $trans, &$data) {
	
	
}









