<?php

/**
 * @file
 *  admin interface for configuring ipay module
 */


/**
 * display iPay configuration form
 *
 * @param array $form_state
 *   eg  2x82,3x14  (signifies 2 of product nid = 82, 3 of product nid = 14)
 * @return
 *   rendered configuration form
 */

function coe_ipay_configure(&$form_state) {
 $form['#title'] = "Configure Setttings and Constants for COE iPay.";
 $form['#prefix'] = ""; 
 coe_ipay_admin_form($form);
 return system_settings_form($form);
}

/**
 * form constructor for iPay configuration
 *
 * @param array $form 
 * @return $form by reference
 */

function coe_ipay_admin_form(&$form) {

 $form['coe_ipay_configure']['coe_ipay_debug_mode'] = array(
  '#type' => 'checkbox',
  '#required' => FALSE,
  '#title' => t('Enable Debug Mode for Site Admins.  Debug will not be visible to all users. '),
  '#default_value' => variable_get('coe_ipay_debug_mode', FALSE), 
  );
 
 $form['coe_ipay_configure']['coe_ipay_test_servers'] = array(
  '#type' => 'checkbox',
  '#required' => FALSE,
  '#title' => t('Use Test iPay Servers'),
  '#description' => t('This will cause all ipay transactions to use the test servers; not just the testing forms.'),
  '#default_value' => variable_get('coe_ipay_test_servers', COE_IPAY_TEST_SERVERS_DEFAULT), 
  );
 
}

// placeholders for future work.

function coe_ipay_store_conf_form_validate($form, &$form_state) {
 
}

function coe_ipay_product_form_validate($form, &$form_state) {

}


