<?php

/**
 * @file
 *
 * this set of functions is designed to work outside of specific web frameworks.  
 * may be needed by other php apps such as drupal, civicrm, joomla, etc.
 * Do not add framework specific function calls to this file.
 *
 * Michael Greifenkamp (2006) wrote much of initial php code for a basic ipay .php page
 * John Barclay (2010) of the College of Education cleaned it up and put into
 *   functions emulating Clint Sterns of ACE's .NET iPay functions and using it with Drupal and Civicrm
 * John Gabel and Tom Scavo (2010) of NCSA cleaned it up some more and implemented it with Joomla and Civicrm
 *   best reference material is Departmental Payment Message Specifications Version 1.1 pdf
 * Marc Henkel is the lead developer on iPay and has facilitated its success by excellent support
 *   and a simple, consistent application.
 *
 * Use these card numbers when doing test transactions.
 * Any expiration date in the future will be valid as well.
 *
 * Visa 4111111111111111
 * Visa 4012888888881881
 * Visa 4222222222222
 * MasterCard 5555555555554444
 * MasterCard 5105105105105100
 * American Express 378282246310005
 * American Express 371449635398431
 * Discover 6011111111111117
 * Discover 6011000990139424
 * 
**/

define('COE_IPAY_TEST_REGISTER_URL','https://webtest.obfs.uillinois.edu/ipay/pc/interfaces/actRegisterCCPaymentNVP.cfm');
define('COE_IPAY_REGISTER_URL','https://www.ipay.uillinois.edu/pc/interfaces/actRegisterCCPaymentNVP.cfm');

define('COE_IPAY_TEST_QUERY_PAYMENT_URL','https://webtest.obfs.uillinois.edu/ipay/pc/interfaces/actQueryCCPaymentNVP.cfm');
define('COE_IPAY_QUERY_PAYMENT_URL','https://www.ipay.uillinois.edu/pc/interfaces/actQueryCCPaymentNVP.cfm');

define('COE_IPAY_CAPTURE_TEST_URL','https://webtest.obfs.uillinois.edu/ipay/pc/interfaces/actCaptureCCPaymentNVP.cfm');
define('COE_IPAY_CAPTURE_URL','https://www.ipay.uillinois.edu/pc/interfaces/actCaptureCCPaymentNVP.cfm');

define('COE_IPAY_TIMESTAMP_FORMAT','m-d-Y H:i:s');
define('COE_IPAY_MESSAGE_DELIMITER', chr(13) . chr(10)  );

define('COE_IPAY_ALLOW_DEBUG', FALSE  );

/**
 * register iPay payment
 *
 * @param int iPay site id
 * @param string $reference_id
 *   internal reference id.  For drupal iPay module, this is transaction nid
 *   could be civicrm event id, etc.
 * @amt string amount of transaction in DD.CC format e.g. 10.50
 * @send_key string send key for registering iPay transaction.  ASCII format
 * @receive_key string receive key for registering iPay transaction.  ASCII format.
 * @test boolean use testing mode.  use test iPay servers
 * @debug boolean make debugging output visible to user
 * @debug_function string name of function to pass debug content to.  Named function
 *   should accept 2 arguments:  a debugging title and a mixed array of arrays and objects.
 *   this function should be good about storing data in session or log rather than displaying
 *   it immediately, as the ipay transaction is likely to have server redirects in it.
 *
 * @return array $response  iPay response array
 */

function coe_ipay_register_cc_payment($ipay_site_id,  $reference_id, $amt, $send_key, $receive_key,
		$test = FALSE, $debug = FALSE, $debug_function = NULL ) {

	$amt = number_format($amt, 2, '.', '');  // requires amount in form 12.31
	$timestamp = gmdate(COE_IPAY_TIMESTAMP_FORMAT); // utc is used to avoid confusion
	$text_to_hash = $amt . $reference_id . $ipay_site_id . $timestamp;
	$certification = _coe_ipay_compute_hash($text_to_hash, $send_key);
	$post_data = _coe_ipay_t("amount=!amt&referenceid=!reference_id&siteid=!cc_site_id&timestamp=!timestamp&certification=!certification",
		array('!amt' => $amt, '!reference_id' => $reference_id, '!cc_site_id' => $ipay_site_id,
			'!timestamp' => $timestamp, '!certification' => $certification));
	$register_url = ($test) ? COE_IPAY_TEST_REGISTER_URL : COE_IPAY_REGISTER_URL;
	$response_text = _coe_ipay_http_post($register_url, $post_data);
	$response = _coe_ipay_parse_response($response_text);
	//dpm('initial parsed response'); dpm($response);
	if ((int)$response['ResponseCode'] == COE_IPAY_SUCCESS ) { // not an error
		$text_to_hash = (string)$response['Redirect'] .
										(string)$response['ResponseCode'] .
										(string)$response['TimeStamp'] .
										(string)$response['Token'];
										
		$check_certification = strtoupper(_coe_ipay_compute_hash($text_to_hash, $receive_key));
    if ( $check_certification != strtoupper($response['Certification'])) {
			$response['ResponseCode'] = COE_IPAY_FAILURE_RECEIVE_MATCH_FAILURE;
		}
		$response['FullRedirectURL'] = $response['Redirect'] ."?Token=". $response['Token'];
	}
	
	
	if ($debug) {
		_coe_ipay_debug_show(
			"coe_ipay_register_cc_payment",
			array("function=coe_ipay_register_cc_payment", "timestamp=$timestamp", "text_to_hash=$text_to_hash",
				"send_key=$send_key","receive_key=$receive_key",
				"sent certification=$certification","post_data=$post_data","register_url=$register_url",
				"check_certification=$check_certification",
				"response_text=$response_text", "response", $response),
			$debug_function);
	}
	
	return $response;
}


/**
 * query iPay payment
 *
 * @param int iPay site id
 * @param string $token iPay token returned from registering query
 * @amt string amount of transaction in DD.CC format e.g. 10.50
 * @send_key string send key for querying iPay transaction.  ASCII format
 * @receive_key string receive key for querying iPay transaction.  ASCII format.
 * @test boolean use testing mode.  use test iPay servers
 * @debug boolean make debugging output visible to user
 * @debug_function string name of function to pass debug content to.  Named function
 *   should accept 2 arguments:  a debugging title and a mixed array of arrays and objects.
 *   
 * @return array $response  iPay response array
 */

 function coe_ipay_query_cc_payment($ipay_site_id, $token, $send_key, $receive_key,
		$test = FALSE, $debug = FALSE, $debug_function = NULL) {
	
	$timestamp = gmdate(COE_IPAY_TIMESTAMP_FORMAT); // utc is used to avoid confusion
	$text_to_hash = $ipay_site_id . $timestamp . $token;
	$certification = _coe_ipay_compute_hash($text_to_hash, $send_key);
	$post_data =  _coe_ipay_t("siteid=!cc_site_id&timestamp=!timestamp&token=!token&certification=!certification",
		array('!cc_site_id' => $ipay_site_id,'!timestamp' => $timestamp,
					'!token' => $token, '!certification' => $certification));

	$query_url = ($test) ? COE_IPAY_TEST_QUERY_PAYMENT_URL : COE_IPAY_QUERY_PAYMENT_URL;
	$response_text = _coe_ipay_http_post($query_url, $post_data);
	$response = _coe_ipay_parse_response($response_text);
	

	if ((int)$response['ResponseCode'] == COE_IPAY_SUCCESS ) {  // ResponseCode, TimeStamp, and TransactionID
		$text_to_hash = (string)$response['ResponseCode'] .
										(string)$response['TimeStamp'] .
										(string)$response['TransactionID'];
										
   	if ($debug) {
			_coe_ipay_debug_show('receive key'. $receive_key, "", $debug_function);					
			_coe_ipay_debug_show('receive text to hash'. $text_to_hash, "", $debug_function);
		}
										
		$check_certification = strtoupper(_coe_ipay_compute_hash($text_to_hash, $receive_key));

    if ( $check_certification != strtoupper($response['Certification'])) {
			$response['ResponseCode'] = COE_IPAY_FAILURE_RECEIVE_MATCH_FAILURE;
		}
	}
	
	if ($debug) {
		_coe_ipay_debug_show(
			"coe_ipay_query_cc_payment",
			array("function=coe_ipay_query_cc_payment", "timestamp=$timestamp", "text_to_hash=$text_to_hash",
				"sent certification=$certification","post_data=$post_data","query_url=$query_url",
				"check_certification=$check_certification",
				"response_text=$response_text", "response", $response),
			$debug_function);
	}
	return $response;
	
 }

/**
 * capture iPay payment
 *
 * @param int iPay site id
 * @param string $token iPay token returned from registering query
 * @accounts array of strings.  list of cfoapals
 * @amounts array of strings.  list of amounts for each cfoapal
 * @send_key string send key for capturing iPay transaction.  ASCII format
 * @receive_key string receive key for capturing iPay transaction.  ASCII format.
 * @test boolean use testing mode.  use test iPay servers
 * @debug boolean make debugging output visible to user
 * @debug_function string name of function to pass debug content to.  Named function
 *   should accept 2 arguments:  a debugging title and a mixed array of arrays and objects.
 *
 * @return array $response  iPay response array
 */

function coe_ipay_capture_cc_payment($ipay_site_id, $token, $accounts, $amounts, $send_key, $receive_key,
		$test = FALSE, $debug = FALSE, $debug_function = NULL) {
	
	$timestamp = gmdate(COE_IPAY_TIMESTAMP_FORMAT); // utc is used to avoid confusion
	$acct_hash = $form_acct_data = "";
	$numaccounts = count($accounts);
	for($i=0; $i < $numaccounts; $i++) {
		$cfoapal = str_replace("-", "", $accounts[$i]);
		$acct_hash .= $cfoapal . number_format($amounts[$i], 2, '.', '');
		$form_acct_data[] = "account". ($i + 1) ."=". $cfoapal ."&amount". ($i + 1) ."=". number_format($amounts[$i], 2, '.', '');

		}
	
	$text_to_hash = $acct_hash . $numaccounts . $ipay_site_id . $timestamp . $token;
	
	$certification = _coe_ipay_compute_hash($text_to_hash, $send_key);
	$post_data =  _coe_ipay_t("siteid=!cc_site_id&token=!token&timestamp=!timestamp&numaccounts=!numaccounts&!acct_form_data&certification=!certification",
		array('!cc_site_id' => $ipay_site_id,'!timestamp' => $timestamp, '!numaccounts' => $numaccounts,
					'!token' => $token, '!acct_form_data' => join('&', $form_acct_data),'!certification' => $certification));
	$capture_url = ($test) ? COE_IPAY_CAPTURE_TEST_URL : COE_IPAY_CAPTURE_URL;
	$response_text = _coe_ipay_http_post($capture_url, $post_data);
	$response = _coe_ipay_parse_response($response_text);

	if ((int)$response['ResponseCode'] == COE_IPAY_SUCCESS ) { // not an error
		$text_to_hash = (string)$response['CaptureAmount'] .
										(string)$response['ResponseCode'] .
										(string)$response['TimeStamp'] .
										(string)$response['TransactionID'];
										
		$check_certification = strtoupper(_coe_ipay_compute_hash($text_to_hash, $receive_key));

    if ( $check_certification != strtoupper($response['Certification'])) {
			$response['ResponseCode'] = COE_IPAY_FAILURE_RECEIVE_MATCH_FAILURE;
		}
	}
	
	if ($debug) {
		_coe_ipay_debug_show(
			"coe_ipay_capture_cc_payment",
			array("function=coe_ipay_capture_cc_payment", "timestamp=". $timestamp, "text_to_hash=". $text_to_hash,
					"certification=".$certification, "post_data=".$post_data, "capture_url=".$capture_url,
					"response_text=".$response_text, "response", $response),
			$debug_function);
	}

	return $response;
	
}

/**
 * post to iPay servers.  requires curl
 *
 * @param url iPay url
 * @param $post_data data going to iPay
 *
 * @return array $response  iPay response array
 */
function _coe_ipay_http_post($url, $post_data) {
	if (! function_exists('curl_init')) die('curl must be installed for this to work');
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);
	curl_setopt($ch, CURLOPT_USERAGENT, $defined_vars['HTTP_USER_AGENT']);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$response = curl_exec($ch);
	curl_close($ch);
	return $response;
	
}

/**
 * parse iPay response
 *
 * @param string $response_text 
 *
 * @return array $response  iPay response array
 */

function _coe_ipay_parse_response($response_text) {
	$lines = explode(COE_IPAY_MESSAGE_DELIMITER, $response_text);
	$items = array();
	foreach ($lines as $line) {
		if ($pair = explode("=",$line)) $items[_coe_ipay_t(trim($pair[0]))] = trim(_coe_ipay_t($pair[1]));
	}
	return $items;
}

/** takes HMAC-SHA1 value of $text_to_hash and $send_key
 *  http://www.php.net/manual/en/function.hash-hmac.php
 *  http://hash.online-convert.com/sha1-generator
 */
function _coe_ipay_compute_hash($text_to_hash, $hash_key) {
	return hash_hmac(SHA1, $text_to_hash, $hash_key);
}


// trimmed down version of drupal t() function
  
function _coe_ipay_t($string, $args = array(), $langcode = NULL) {

    // Transform arguments before inserting them.
    foreach ($args as $key => $value) {
      switch ($key[0]) {
        case '@':
				case '%':
          // Escaped only.
          $args[$key] = _coe_ipay_check_plain($value);
          break;
        case '!':
          // Pass-through.
      }
    }
    return strtr($string, $args);
}

function _coe_ipay_check_plain($text) {
	_coe_ipay_validate_utf8($text) ? htmlspecialchars($text, ENT_QUOTES) : '';
}

function _coe_ipay_validate_utf8($text) {
  if (strlen($text) == 0) {
    return TRUE;
  }
  return (preg_match('/^./us', $text) == 1);
}

function _coe_ipay_error($i) {
	static $errors;
	if (!$errors) $errors = coe_ipay_errors();
	return $errors[$i];
}


define('COE_IPAY_SUCCESS', 0);
define('COE_IPAY_FAILURE_REQUEST_ATTRIBUTE_PARSE_ERROR',1);
define('COE_IPAY_FAILURE_CERTIFICATION_MATCH_FAILURE',2);
define('COE_IPAY_FAILURE_SITE_NOT_ACTIVE',3);
define('COE_IPAY_FAILURE_TENDER_MISMATCH',4);
define('COE_IPAY_FAILURE_TRANSACTION_TOKEN_EXPIRED',5);
define('COE_IPAY_FAILURE_AUTHORIZATION_FAILURE',6);
define('COE_IPAY_FAILURE_TOKEN_NOT_FOUND',7);
define('COE_IPAY_FAILURE_CAPTURE_FAILURE',8);
define('COE_IPAY_FAILURE_TIMESTAMP_OFFSET_TOO_LARGE',9);
define('COE_IPAY_FAILURE_MISMATCHED_AUTH_AMOUNT',10);
define('COE_IPAY_FAILURE_SYSTEM_ERROR',100);
define('COE_IPAY_FAILURE_RECEIVE_MATCH_FAILURE',200);
define('COE_IPAY_FAILURE_LOCAL_ERROR',300);


function _coe_ipay_errors($i) {
	$e[0] =  'Success';
	$e[1] =  'Failure � Request Attribute Parse Error';
	$e[2] =  'Failure � Certification Match Failure';
	$e[3] =  'Failure � Site Not Active';
	$e[4] =  'Failure � Tender Mismatch';
	$e[5] =  'Failure � Transaction Token Expired';
	$e[6] =  'Failure � Authorization Failure';
	$e[7] =  'Failure � Token Not Found';
	$e[8] =  'Failure � Capture Failure';
	$e[9] =  'Failure � TimeStamp offset too large';
	$e[10] =  'Failure � Mismatched Auth Amount';
	$e[100] =  'Failure � System Error';
	$e[200] =  'Failure � Receive Match Error';
	$e[300] =  'Failure � Local Error';
	
	return $e[$i];
}



function _coe_ipay_debug_show($title, $data, $debug_function = '_coe_ipay_debug_show_simple') {
	if (COE_IPAY_ALLOW_DEBUG && function_exists($debug_function) ) {
		call_user_func($debug_function, $data, $title);
	}
}

function _coe_ipay_debug_show_simple($data = NULL, $title = NULL) {
	if ($title) {print "<h2>$title</h2>"; }
	if ($arg) {print "<pre>"; print_r($data); print "</pre>";  }
}






