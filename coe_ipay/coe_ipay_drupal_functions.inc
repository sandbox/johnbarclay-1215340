<?php


/**
 * @file
 *  coe_ipay_drupal_functions:  set of functions specific to drupal and iPay/iPay
 *    works with cck site, product and transaction content types.
 */

require_once('coe_ipay_drupal_crud.inc'); 

/**
 * Register a transaction with ipay and store transaction data in cck.
 *
 * @param $ipay_store
 *   An ipay_store site definition as array or a handle or node id
 * @param array $products_chosen
 *   Productions being purchased.
 * @param array $data
 *   Additional data to be stored in transaction table in data field
 * @param bool $test
 *   In test mode?  Will use test servers.
 * @param bool $debug 
 *   display debugging information
 *
 * @return array $response
 *   array in form of ipay response as defined by Departmental Payment Message Specifications
 *   'ResponseCode', 'Token', 'Redirect', 'TimeStamp', 'Certification'
 */

function coe_ipay_register(&$ipay_store, &$products_chosen, &$data, $test = FALSE, $debug = FALSE) {
  if (variable_get('coe_ipay_test_servers', COE_IPAY_TEST_SERVERS_DEFAULT)) {$test = TRUE;}
  require_once('coe_ipay.api.inc');
	//dpm('coe_ipay_register products_chosen'); dpm($products_chosen);
  list($trans_nid, $amt) = coe_ipay_trans('create', $ipay_store, NULL, $products_chosen, $data, NULL, $test, $debug);
  list($ipay_site_id, $send_key, $receive_key) = _coe_ipay_get_core_site_params($ipay_store['node'], $test);

  $response = coe_ipay_register_cc_payment($ipay_site_id, $trans_nid, $amt, $send_key, $receive_key, $test, $debug, COE_IPAY_DEBUG_FUNCTION);
	//dpm('coe_ipay_register 2 products_chosen'); dpm($products_chosen);
  coe_ipay_trans('register', $ipay_store, $trans_nid, NULL, NULL, $response, $test, FALSE);

  $_SESSION['coe_ipay_trans_nid'] = $trans_nid;
  
  _coe_ipay_debug_show("",array("function=coe_ipay_register", "cc_site node", $ipay_store,
				"trans_nid=$trans_nid",
				"amt=$amt", "send_key=$send_key", "receive_key=$receive_key",
				"response_text=$response_text", "response", $response),
				COE_IPAY_DEBUG_FUNCTION
		);
	
  
  return $response;

}

/**
 * Finish a transaction (Query and Capture) with ipay and store transaction data in cck.
 *
 * @param string $token
 *   A ipay token returned from ipay registration transaction
 * @param bool $test
 *   In test mode?  Will use test servers.
 * @param bool $debug 
 *   display debugging information
 *
 * @return array $response
 *   array in form of ipay response as defined by Departmental Payment Message Specifications
 *   which attributes are present depends on success of query and capture.
 *   'ResponseCode', 'TransactionID', 'Redirect', 'TimeStamp', 'Certification', 'CaptureAmount'
 */


function coe_ipay_finish($token, $test = FALSE, $debug = FALSE, $data = array()) {
  if (variable_get('coe_ipay_test_servers', COE_IPAY_TEST_SERVERS_DEFAULT)) {$test = TRUE;}
  require_once('coe_ipay.api.inc');
  $trans_node = node_load($_SESSION['coe_ipay_trans_nid'], NULL, TRUE);
	//dpm($trans_node);
	//dpm($token . "=" . $trans_node->field_ipay_trans_pc_token[0]['value']);
  if ($token != $trans_node->field_ipay_trans_pc_token[0]['value']) {
    drupal_set_message(IPAY_SITE_BAD_TOKEN,"error");
		return FALSE; 
  }
  $test = $trans_node->field_ipay_trans_is_test[0]['value'];
	
  $ipay_store = coe_ipay_store_from_nid($trans_node->field_ipay_trans_site_id[0]['nid']);
  list($ipay_site_id, $send_key, $receive_key) = _coe_ipay_get_core_site_params($ipay_store['node'], $test);
   
	$response = coe_ipay_query_cc_payment($ipay_site_id, $token, $send_key, $receive_key, $test, $debug, COE_IPAY_DEBUG_FUNCTION); 
	coe_ipay_trans('query', $ipay_store, $trans_node->nid, NULL, $data, $response, $test, $debug);

  _coe_ipay_debug_show("", array("function=coe_ipay_finish (query part)", "trans node",
		$trans_node, 
		"test=$test", "ipay_store", $ipay_store, "send_key=$send_key", "receive_key=$receive_key",
		"response", $response),
		COE_IPAY_DEBUG_FUNCTION
		);
	
  
  if ($response['ResponseCode'] == COE_IPAY_SUCCESS) {
    list($accounts, $amounts, $subtrans) = coe_ipay_get_subtrans($trans_node->nid, $debug);
    
    $response = coe_ipay_capture_cc_payment($ipay_site_id, $token, $accounts, $amounts, $send_key, $receive_key, $test, $debug, COE_IPAY_DEBUG_FUNCTION);
		coe_ipay_trans('capture', $ipay_store, $trans_node->nid, NULL, NULL, $response, $test, $debug,  COE_IPAY_DEBUG_FUNCTION);
    if ($response['ResponseCode'] == COE_IPAY_SUCCESS) {
      unset($_SESSION['coe_ipay_trans_nid']);
    }
    
  }
  
  $response['coe_ipay_trans_nid'] = $trans_node->nid;

	_coe_ipay_debug_show("", array("function=coe_ipay_finish (capture part)", "trans node", $trans_node, 
				"test=$test", "ipay_store", $ipay_store, "send_key=$send_key", "receive_key=$receive_key",
				"subtrans", $subtrans, "response", $response), COE_IPAY_DEBUG_FUNCTION);
	
  
  return $response;
}

/**
 * helper function to parse $cc_site node
 */
function _coe_ipay_get_core_site_params($ipay_store_node, $test) {

  $ipay_site_id = $ipay_store_node->field_ipay_site_id[0]['value'];
  $receive_key = ($test) ? $ipay_store_node->field_ipay_site_rec_test[0]['value']  : $ipay_store_node->field_ipay_site_receive_key[0]['value'];
	$send_key = ($test) ? $ipay_store_node->field_ipay_site_snd_test[0]['value']  : $ipay_store_node->field_ipay_site_send_key[0]['value'];
  $ret = array($ipay_site_id, $send_key, $receive_key);
  return $ret;
}


/**
 * record the transaction and subtrans and related data from iPay response data.
 *
 * @param string $op = create, register, query, capture
 *   ipay query just committed (or create to create transaction node)
 * @param node $ipay_site
 *   ipay site node
 * @param string $trans_nid
 *   node id of transaction
 * @param array $products_chosen
 *   array of products purchased.
 * @param array $data
 *   array of additional data to be merged with generic data for node.  Careful about array key collisions. 
 * @param bool $test
 *   In test mode?  Will use test servers.
 * @param bool $debug 
 *   display debugging information
 *
 * @return array $trans_nid, $amt
 *   for create or query, returns transaction id and total amount of transaction
 */


function coe_ipay_trans($op, &$ipay_store, $trans_nid, $products_chosen, $data, $response, $test, $debug = FALSE) {
  require_once('coe_ipay.api.inc');
	//dpm($op); dpm($response); 
  _coe_ipay_debug_show('coe_ipay_trans, op='. $op, NULL, COE_IPAY_DEBUG_FUNCTION);
  global $user;
			
  if ($op == 'create') {
    $trans_node = new StdClass();
    $trans_node->type = 'ipay_trans';
		
    $trans_node->field_ipay_trans_site_id[0]['nid'] = $ipay_store['node']->nid;
    $trans_node->field_ipay_trans_status[0]['value'] = "created";
    $trans_node->field_ipay_trans_is_test[0]['value'] = $test;
		$trans_node->field_ipay_trans_uid[0]['value'] = $user->uid;
		$trans_node->uid = 1; // super admin owns all transactions
  } else {
    $trans_node = node_load($trans_nid, NULL, TRUE); // must reset cache here because must have most recent node
  }
  _coe_ipay_debug_show('tran to be updated or created', $trans_node, COE_IPAY_DEBUG_FUNCTION); 
  if ($data) {
    if ($trans_node->field_ipay_trans_data[0]['value']) {
      $data = array_merge($data, unserialize($trans_node->ipay_trans_data[0]['value']));
    }
    $trans_node->field_ipay_trans_data[0]['value'] = serialize($data);
  }
  
  if ($op == 'register') {
    if ($response['ResponseCode'] == COE_IPAY_SUCCESS) {
      $trans_node->field_ipay_trans_status[0]['value'] = "registered";
      $trans_node->field_ipay_trans_pc_token[0]['value'] = $response['Token'];
    } else {
      $trans_node->field_ipay_trans_status[0]['value'] = "register_error";
      $trans_node->field_ipay_trans_error_id[0]['value'] = $response['ResponseCode'];
    }
  }
  elseif ($op == 'query') {
    if ($response['ResponseCode'] == COE_IPAY_SUCCESS) {
      $trans_node->field_ipay_trans_status[0]['value'] = "queried";
      $trans_node->field_ipay_trans_ipay_id[0]['value'] = $response['TransactionID'];
    } else {
      $trans_node->field_ipay_trans_status[0]['value'] = "query_error";
      $trans_node->field_ipay_trans_error_id[0]['value'] = $response['ResponseCode'];
    }    
  }
  
  elseif ($op == 'capture') {
    if ($response['ResponseCode'] == COE_IPAY_SUCCESS) {
      $trans_node->field_ipay_trans_status[0]['value'] = "captured";
      $trans_node->field_ipay_trans_pay_tot[0]['value'] = $response['CaptureAmount'];
    } else {
      $trans_node->field_ipay_trans_status[0]['value'] = "capture_error";
      $trans_node->field_ipay_trans_error_id[0]['value'] = $response['ResponseCode'];
    }    
  }
  $trans_node->title = 'iPay Transaction: '. $ipay_store['node']->title .'.  Transaction: '. $trans_nid; 
  //dpm('trans node before initial save'); dpm($trans_node);
	node_save($trans_node);
  if ($op == 'create') {$trans_nid = $trans_node->nid; } 
 
  if (count($products_chosen) && $op == 'create') {  // either update or insert product records
    _coe_ipay_debug_show("products Chosen", $products_chosen, COE_IPAY_DEBUG_FUNCTION);
    $amt = NULL;
    
    foreach($products_chosen as $product_nid => $product) {
		//	dpm($product);
      $subtrans_node = new StdClass();
      $subtrans_node->type = 'ipay_subtrans';
			$subtrans_node->title = 'iPay Subtransaction. Product: '. $product_nid .'.  Transaction: '. $trans_nid;
			$subtrans_node->uid = 1; // super admin owns all transactions
      $subtrans_node->field_ipay_strn_trans_i[0]['nid'] = $trans_nid;
      $subtrans_node->field_ipay_strn_prdt_id[0]['nid'] = $product_nid;
      $subtrans_node->field_ipay_strn_price[0]['value'] = $product['unit_price'];
      $subtrans_node->field_ipay_strn_qty[0]['value'] = $product['qty'];
      $subtrans_node->field_ipay_strn_cc_amt[0]['value'] = $product['qty'] * $product['unit_price'];
			$subtrans_node->field_ipay_strn_cfoapal[0]['cfoapal'] = $product['cfoapal'];
      
			_coe_ipay_debug_show("coe_ipay_trans", $ipay_store, COE_IPAY_DEBUG_FUNCTION);
			_coe_ipay_debug_show("coe_ipay_subtrans", $subtrans_node, COE_IPAY_DEBUG_FUNCTION);
				
			// @todo get this back in working order
     // foreach(array('coa_cd','fin_fund_cd','org_cd','fin_acct_cd','fin_pgm_cd','fin_actv_cd') as $cfoptag) {
       // $field_name = "field_ipay_strn_". $cfoptag;
				
       // $subtrans_node->{$field_name}[0]['value'] =
        //  $ipay_store['products'][$product_nid][$cfoptag];
     //  }
      $amt = $amt + $product['qty'] * $product['unit_price'];
      $subtrans_node = node_save($subtrans_node);
//			dpm($subtrans_node); dpm( $product['cfoapal']);
    }
  }

  if ($op == 'create' || $op == 'query') {
    return array($trans_nid, $amt);
  }

}



/** give misc logging data for ipay trans **/
function coe_ipay_context_data() {
  static $data;
  
  if (!isset($data)) {
    $data = array();
    global $user;
    foreach(array('HTTP_USER_AGENT','QUERY_STRING','PATH','REMOTE_ADDR') as $prop) {
      $data[$prop] = $_SERVER[$prop];
    }
    if ($user->uid != 0) { // authenticated user
      $data['uid'] = $user->uid;
    }
  }
  
  return $data;
}




/** given ipay response error, show error to user with link to cancel page  **/
function coe_ipay_response_show_error($response, $show_cancel_url = TRUE, $stage) {
  
  if ($response['self_cancel']) {
    drupal_set_message("Transaction not completed", "warning");
  }
  else {
		require_once('coe_ipay.api.inc');
    $error_text = _coe_ipay_errors($response['ResponseCode']);
    $text = "Transaction not completed.  Please try again. ($error_text)";
    
    if ($show_cancel_url && $response['cancelURL']) {
      $text .= l("Return to registration", $response['cancelURL']);
    }
     drupal_set_message($text, "error");
  }

}



/**
 * called from menu system directly to initiate an iPay transaction
 *   and redirect to iPay directly.
 *
 * @param string $ipay_site_id
 *   the particlar coe ipay site id; not the 3 digit one from iPay
 * @param int $products_arg
 *   eg  2x82,3x14  (signifies 2 of product nid = 82, 3 of product nid = 14)
 *   
 * @TODO:  verify that referrer is same hostname as site or in allowable
 *  referrers in site configuration
 */

function coe_ipay_passthrough_start($ipay_store_tag, $products_arg, $test) {
  
  $debug = variable_get('coe_ipay_debug_mode', FALSE);
  require_once('coe_ipay.api.inc');
  _coe_ipay_debug_show("coe_ipay_passthrough_start",
    " products_arg=$products_arg, ipay_store_tag=$ipay_store_tag" , COE_IPAY_DEBUG_FUNCTION);
   

  if (!($store =  coe_ipay_store_from_nid(coe_ipay_store_tag_to_nid($ipay_store_tag)))) {
    drupal_set_message("store for $ipay_store_tag not found",'error');
    return;
  }
  
    
 // $site_array_flat = array_values($sites_array);
 // $site = coe_ipay_store_from_nid($site_array_flat[0]['site_nid']);
  _coe_ipay_debug_show("<h2>site data</h2>", $store);
  
  /** finish this by using regex.  exact match on partial hostname will not work
  $url_parts = parse_url($_SERVER['HTTP_REFERER']));
  $allowed_referrers = explode("\n", $site['allowed_refer']);
  $allowed_referrers[] = $base_url;
  if (! in_array($url_parts['host'], $allowed_referrers)) {
    drupal_set_message(
      t('Transactions originating from !refererer are not allowed',
        array('!referrer' => $$url_parts['host'])),
      'error');
  }
  **/
  
  $products = _coe_ipay_parse_products($products_arg, $store);
//	dpm('products in coe_ipay_passthrough_start'); dpm($products);
  if (count($products) == 0) {
    drupal_set_message('This page reached in err.  Products must be included in url.','error');
    return "";
  }
  
  _coe_ipay_debug_show("<h2>products</h2>", $products, COE_IPAY_DEBUG_FUNCTION); 
  $data = coe_ipay_context_data();
	//print "<pre>"; print_r($data); die;
	global $slippers_dev;
  $response = coe_ipay_register($store, $products, $data, $test, $debug);

  if ($response['ResponseCode'] == COE_IPAY_SUCCESS && $response['FullRedirectURL']) {
    drupal_goto($response['FullRedirectURL']);
  } else {
    _coe_ipay_debug_show("<h2>response array</h2>", $response, COE_IPAY_DEBUG_FUNCTION);
    coe_ipay_response_show_error($response, TRUE, 'end');
  }
  return "Receipt Page";
}

/**
 * called from menu system directly to initiate an iPay transaction
 *   and redirect to iPay directly.
 *
 * @param string $products_arg
 *   eg  2x82,3x14  (signifies 2 of product nid = 82, 3 of product nid = 14)
 * @param array $site
 *   array defining iPay drupal site
 * @return
 *   The array of products keyed on product nid
 */

function _coe_ipay_parse_products($products_arg, &$store) {
  $products_arg = check_plain($products_arg);
  $pairs = explode(",",trim(strtolower($products_arg)));
  
  foreach($pairs as $pair) {
    list($qty, $product_nid) = explode("x",$pair);
		$products[$product_nid] = coe_ipay_simple_prodt_array($store, $qty, $product_nid);
  }
  return $products;
}

function coe_ipay_simple_prodt_array(&$store, $qty, $product_nid = NULL) {
	 if ($product_nid) {
      $product['nid'] = $product_nid;  // this will give cfop, otherwise will default to site ccfoapal
      $product['unit_price'] = $store['products'][$product_nid]->field_ipay_prdt_price[0]['value'];
      $product['qty'] = $qty;
			$product['cfoapal'] = $store['products'][$product_nid]->field_ipay_prdt_cfoapal[0]['cfoapal'];
    }
	
	return $product;
}



/*
 * receipt
 * 
 * @param string $ipay_site_id
 *   the iPay site id.  The one issued when you apply for the site
 * @param int $site_nid
 *   the node id of the drupal coe_ipay content type
 * @TODO
 *   figure out exactly what needs to be done by content module when
 *   field modules are installed, uninstalled, enabled or disabled.
 */

function coe_ipay_receipt() {
	
	dpm("coe_ipay_receipt");
  require_once('coe_ipay.api.inc');

  if (!($token = $_GET['token'])) {
    drupal_set_message("You have come to this page in error.  A token and site id
      should be in the url.","error");
    return "<h2>iPay Error Encountered</h2>";
  }
  
  $trans_nid = $_SESSION['coe_ipay_trans_nid']; // this will be cleared from session after capture
  
  $debug = variable_get('coe_ipay_debug_mode', FALSE);
  $response = coe_ipay_finish($token, NULL, $debug);
  if ($response  === FALSE) {
    return " ";
  }
  $trans_node = node_load($trans_nid, NULL, TRUE);
	$ipay_trans_data = unserialize($trans_node->field_ipay_trans_data[0]['value']);
	$store_nid = $trans_node->field_ipay_trans_site_id[0]['nid'];
  list($discard1, $discard2, $subtrans) = coe_ipay_get_subtrans($trans_nid);
  _coe_ipay_debug_show("trans node", $trans_node, COE_IPAY_DEBUG_FUNCTION);
	_coe_ipay_debug_show("subtrans node", $subtrans, COE_IPAY_DEBUG_FUNCTION);
	_coe_ipay_debug_show("ipay transaction data", $ipay_trans_data, COE_IPAY_DEBUG_FUNCTION);
  /** transaction is finished at this point.  there are three possible outcomes:
   *   forward to success or fail url
   *   show success or fail template
   *   show generic success or fail template
   **/
  
  //$site_node = node_load($site_nid);
  $store_data = coe_ipay_store_from_nid($store_nid);
	$store_node = $store_data['node'];
 // dpm($site_data);
 
  _coe_ipay_debug_show("store node", $store_node, COE_IPAY_DEBUG_FUNCTION);
	
	$success = ((int)$response['ResponseCode'] === 0);
	if ($success && isset($ipay_trans_data['civicrm_trans']['returnURL'])) {
		$goto = $ipay_trans_data['civicrm_trans']['returnURL'];
	} elseif ($success) {
		$goto = $store_node->field_ipay_site_success_url[0]['value'];
	} elseif (isset($ipay_trans_data['civicrm_trans']['cancelURL'])) {
		$goto = $ipay_trans_data['civicrm_trans']['cancelURL'];
	} else {
		$goto = $store_node->field_ipay_site_cancel_url[0]['value'];
	}
  
 // dpm($site_node);
  if ($success) {
    $tpl = ($store_node->field_ipay_site_success_tpl[0]['value']) ? $store_node->field_ipay_site_success_tpl[0]['value'] : "Successful Transaction";
  } else {
    $tpl = ($store_node->field_ipay_site_fail_tpl[0]['value']) ? $store_node->field_ipay_site_fail_tpl[0]['value'] : "Error Has Occurred";
  } 
  
  $tokens = coe_ipay_receipt_tokens($response, $subtrans, $store_data);
	//dpm($tokens);
  $content = token_replace($tpl, 'coe_ipay', $tokens);
  
  if ($goto) {  // if destination is set and template, template is put in message area
    if ($tpl) {
      $type = ($success) ? "status" : "error";
      drupal_set_message($content, $type);
    }
    drupal_goto($goto);
  } 
  $debug = variable_get('coe_ipay_debug_mode', FALSE);
  if ($debug) {
    $show = array(
      "Last iPay/iPay Response" =>  $response,
      "Site Defition Node" => node_load($trans_node->field_ipay_trans_site_id[0]['nid'], NULL, TRUE),
      "Transaction Node" => $trans_node,
      "Sub Transactions"  => $subtrans,
    );
      
      
    // need to show all transaction data
     _coe_ipay_debug_show("Transaction Summary", $show, COE_IPAY_DEBUG_FUNCTION);
    
     ob_start();
     print_r($show);
     $show_pre = ob_get_contents();
     ob_end_clean();
     
     _coe_ipay_debug_show("Transaction Summary", $show_pre, COE_IPAY_DEBUG_FUNCTION);
     
    $content .= "<pre>". $show_pre . "</pre>";
  }
  return $content;
}

/*
 * tokens for iPay receipt templates
 * 
 * @param array $response
 * @param array $subtrans
 * @param array $site_data
 * @return associative array of token/value pairs.
 */

function coe_ipay_receipt_tokens($response, $subtrans, $store_data) {
  
  //dpm('tokens'); dpm($response);  dpm($subtrans); dpm($store_data);
  $tokens['capture_amount'] = $response['CaptureAmount'];
  $tokens['transaction_id'] = $response['TransactionID'];
  $tokens['timestamp'] = $response['TimeStamp'];
  $tokens['drupal_trans_nid'] = $response['coe_ipay_trans_nid'];
  $tokens['response_code'] = (int)$response['ResponseCode'];
  $tokens['ipay_credit_card_stmt'] = COE_IPAY_CC_STMT_MSG;
  
  $rows = array();
  foreach($subtrans as $subtrans_nid => $subtrans) {
   // $product_nid = $subtrans['field_ipay_strn_prdt_id_nid'];  
    $rows[] = array(
      $subtrans['field_ipay_strn_qty_value'],
      $subtrans['field_ipay_prdt_name_value'],
      "$" . $subtrans['field_ipay_strn_cc_amt_value'],
    );
    
  }
  $headers = array("Quantity", "Name", "Amount");
  $tokens['products_list'] =  theme_table($headers, $rows, array(), NULL);

  
  if ((int)$response['ResponseCode'] === 0) {
    $tokens['response_code_text'] = "";
   $tokens['response_code_state'] = "Success";
  } else {
    $parts = explode(" - ",_coe_ipay_errors($response['ResponseCode']));
    $tokens['response_code_text'] = (count($parts) > 0) ? $parts[1] : "";
    $tokens['response_code_state'] = "Failure";
  }
  
  return $tokens;
}




?>