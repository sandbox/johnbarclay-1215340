<?php


function coe_ipay_drupal_test_forms($site_tag = NULL) {
	//dpm($site_tag);
  if ($site_tag) {
    return drupal_get_form('coe_ipay_drupal_test_form', $site_tag);
  } else {
    return coe_ipay_drupal_test_form_index();
  }
}


function coe_ipay_drupal_test_form_index() {
  require_once(drupal_get_path('module','coe_ipay') .'/coe_ipay_drupal_functions.inc');
  
  $sites = coe_ipay_stores(TRUE);
//	dpm('sites data'); dpm($sites);
  foreach ($sites as $site_nid => $site_defn) {
		//dpm($site_defn);
    $list[] = l($site_defn['node']->title, "admin/settings/ipay/testforms/". $site_defn['node']->field_ipay_site_tag[0]['value']);
  }
  $content = theme_item_list($list, "iPay stores for testing");
  return $content;
}   



function coe_ipay_drupal_test_form(&$form_state, $site_tag) {
  require_once(drupal_get_path('module','coe_ipay') .'/coe_ipay_drupal_functions.inc');
  
  if(!($site = coe_ipay_store_from_nid(coe_ipay_store_tag_to_nid($site_tag)))) {
   $message = t('<em>!ipay_site_nid</em> does not match a known ipay site',
     array('!ipay_site_nid' => $site_nid));
    drupal_set_message($message, "error");         
  }
	//dpm($site);
  $form['preamble'] =
     array(
        '#type' => 'markup',
        '#value' => "Enter a few product quantities to test for the $site_id ipay site.",
        '#weight' => 0
        );

  $form['use_test_server'] = array(
    '#type' => 'checkbox',
    '#required' => FALSE,
    '#title' => t('Use Test iPay Server?'),
    '#description' => "Not using this will mean some real credit card transactions",
    '#default_value' => 1, 
    );
  
  if(count($site['products']) == 0) {
   $message = t('<em>!site_tag</em> has no products associated with it.
    you must create some products for test forms to work.',
     array('!site_tag' => $site_tag));
    drupal_set_message($message, "error");         
  }
  
  if ($site['products']) {
		//dpm($site['products']);
    foreach($site['products'] as $product_id => $product) {
		//	print $product_id;
      $form['fieldset_'. $product_id]["qty_". $product_id] = array(
        '#size' => 3,
        '#type' => 'textfield',
        '#title' => t("Quantity of %name at price (id=product_nid)", array('%name' => $product->title)),
				'#default_value' => 1,
      );
    }
  }
 
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => "Test",
  );
  
  if ($site['products']) {
    $passthroughs = array();
    foreach($site['products'] as $product_id => $product) {
      $passthroughs[] = rand(1,3) . "x" . $product_id;
      }
    $url = join("/",
      array(
        'ipay/passthrough',
        $site_tag,
        join(",",$passthroughs)
      )
    );
 
   $form['passthrough_url'] = array(
      '#type' => 'markup',
      '#title' => t('Passthrough test/example url:'),
      '#value' => "<hr/><h2>Passthrough test/example url:</h2>" . l("$url", $url)
    );
  }
  
  
  $form['#validate'][] = 'coe_ipay_drupal_test_form_validate';
  $form['#submit'][] = 'coe_ipay_drupal_test_form_submit';
  
  return $form;

}


function coe_ipay_drupal_test_form_validate(&$form, &$form_state) {
  require_once(drupal_get_path('module','coe_ipay') .'/coe_ipay_drupal_functions.inc');
    
 if (!($site = coe_ipay_store_from_nid(coe_ipay_store_tag_to_nid(arg(4))))) {
    form_set_error('ipay','Site '. arg(4) . ' is not found');
    return;
  }

  list($cost, $products) = coe_ipay_parse_test_form($form_state, $site);
  
  if ($cost == 0) {
     form_set_error('ipay',"Invalid total calculated: $cost");
  }

}


function coe_ipay_drupal_test_form_submit(&$form, &$form_state) {
  require_once(drupal_get_path('module','coe_ipay') .'/coe_ipay_drupal_functions.inc');
  // store any data in ipay trans and sub trans
  $site = coe_ipay_store_from_nid(coe_ipay_store_tag_to_nid(arg(4)));
  $data = coe_ipay_context_data();
  $debug = variable_get('coe_ipay_debug_mode', FALSE);
  list($cost, $products) = coe_ipay_parse_test_form($form_state, $site);
  
  
 // print $debug; die;
  $response = coe_ipay_register($site, $products, $data, $form_state['values']['use_test_server'], $debug);
  if ($response['FullRedirectURL']) $form['#redirect'] = $response['FullRedirectURL'];

}

function coe_ipay_parse_test_form(&$form_state, &$site) {
  require_once(drupal_get_path('module','coe_ipay') .'/coe_ipay_drupal_functions.inc');
  
  $cost = 0;
  foreach($form_state['values'] as $name => $qty) {
    if (strpos($name,'qty_') === 0) { // item is a product
      list($discard, $product_nid) = explode('_', $name);
      if (!$site['products'][$product_nid]) {
        $errors[] = "Product id $product_id not found in ipay site ". $node->title;
      } else {
			//	dpm('site!!!'); dpm($site);
				$return_products[$product_nid] = coe_ipay_simple_prodt_array($site, $qty, $product_nid);
        $cost = $cost + (int)$qty * $return_products[$product_nid]['unit_price'];
     }
    }
    
  }
  return array($cost, $return_products);
}


