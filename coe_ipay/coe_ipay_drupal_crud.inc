<?php


/**
 * @file
 *  coe_ipay_crud functions:  set of functions specific to drupal and iPay/iPay
 *    works with cck site, product and transaction content types.
 */


function coe_ipay_get_published_nids($content_type) {
	$sql = "SELECT node.nid as nid
		FROM content_type_$content_type S
		LEFT OUTER JOIN {node} on node.nid = s.nid
		WHERE status = 1";
		//dpm($sql);
	$result = db_query($sql);
	$nids = array();
	while ($node = db_fetch_array($result)) {
		$nids[] = $node['nid'];
	}

	return $nids;
}
/**
 * get array of iPay sites defined in ipay content type
 *
 * @param bool $reset 
 *   clear cache and requery sites.
 *
 * @return array $sites
 *   keyed on site handle with attributes from site cck node.
 */

function coe_ipay_stores($reset = FALSE) {

/** 
  static $sites;
	
	if ($sites && $reset == FALSE) { // static variable set
		return $sites;
	}
	
	if ($reset == FALSE) { // cached array returned
		$sites_cache = cache_get(COE_IPAY_STORES_CACHE_ID);
		$sites = $site_cache->data;
		if (is_array($sites) ) {
			return $sites;
		}
	}
	**/

	// get sites data
	

	$store_nids = coe_ipay_get_published_nids('ipay_store') ;
	//dpm('published ipay_site');  dpm($store_nids);
	// get sites
	foreach ($store_nids as $store_nid) {
		$store_node = node_load($store_nid);
	//	dpm($site_node);
		$stores[$store_nid]['node'] = $store_node;
		$store_tags[$store_node->field_ipay_site_tag[0]['value']] = $store_nid;
	}

	// get products
	$product_nids = coe_ipay_get_published_nids('ipay_prd');
	foreach ($product_nids as $product_nid) {
		$product_node = node_load($product_nid);
		//dpm($product_node);
		$store_nid = $product_node->field_ipay_prdt_site_nid[0]['nid'];
		$default_cfoapal = $stores[$store_nid]['node']->field_ipay_prdt_cfoapal[0]['cfoapal'];
		if (!$product_node->field_ipay_prdt_cfoapal[0]['cfoapal']) {
			$product_node->field_ipay_prdt_cfoapal[0]['cfoapal'] = $default_cfoapal;
		}
		$stores[$store_nid]['products'][$product_nid] = $product_node;
	}

	//dpm('sites cache'); dpm($sites);
	//dpm('sites tags cache'); dpm($site_tags);
	cache_set(COE_IPAY_STORES_CACHE_ID, $stores);
	cache_set(COE_IPAY_STORE_TAGS_CACHE_ID, $store_tags);
	return $stores;
	
}

/**
 * get array of a single ipay site
 *
 * @param string $site_id 
 *   site node_id or site tag.
 *
 * @param bool $reset 
 *   clear cache and requery site data.
 *
 * @return array $sites
 *   keyed on site handle with attributes from site cck node.
 */

function coe_ipay_store_tag_to_nid($site_tag) {
	$sql = "SELECT nid FROM {content_type_ipay_store} WHERE field_ipay_site_tag_value = '%s'";
	$site = db_fetch_array(db_query($sql, $site_tag));
	return $site['nid'];
	
}
function coe_ipay_store_from_nid($site_nid, $reset = TRUE) {
  $sites = coe_ipay_stores($reset);
	return $sites[$site_nid];
}

/**
 * helper function to return all subtransactions
 */
function coe_ipay_get_subtrans($trans_nid, $debug = TRUE) {
	require_once('coe_ipay.api.inc');
		
  $rows = db_query(
		"SELECT content_type_ipay_subtrans.nid as nid, 
content_type_ipay_prd.field_ipay_prdt_cfoapal_cfoapal,
content_type_ipay_subtrans.field_ipay_strn_cc_amt_value,
content_type_ipay_subtrans.field_ipay_strn_qty_value,
content_type_ipay_subtrans.field_ipay_strn_price_value,
content_type_ipay_prd.field_ipay_prdt_name_value,
content_type_ipay_prd.field_ipay_prdt_fk_name_value,
content_type_ipay_prd.field_ipay_prdt_fk_value_value,
content_type_ipay_prd.field_ipay_prdt_price_value
FROM {content_type_ipay_subtrans}
		LEFT OUTER JOIN {content_type_ipay_prd}
		ON content_type_ipay_prd.nid = content_type_ipay_subtrans.field_ipay_strn_prdt_id_nid
		WHERE field_ipay_strn_trans_i_nid = %d", $trans_nid
									 );
  $amounts = array();
  $subs = array();
  while($subtransaction = db_fetch_array($rows)) {
    $subtransaction_nid = $subtransaction['nid'];
    $subs[$subtransaction_nid] = $subtransaction;
    $ccfoapal =  coe_ipay_get_cfoapal($subtransaction, 'subtrans'); // $subtransaction['field_ipay_strn_ccfoapal_value'];
    $amounts[$ccfoapal] = $amounts[$ccfoapal] + $subtransaction['field_ipay_strn_cc_amt_value'];
  }
  
  _coe_ipay_debug_show('_coe_ipay_parse_subtrans: sub transactions', $subs, COE_IPAY_DEBUG_FUNCTION);
  return array(array_keys($amounts), array_values($amounts), $subs);
}

/**
 * return cfoapal string for a subtransaction
 */
function coe_ipay_get_cfoapal($subtransaction, $mode = 'subtrans') {
	if ($mode == 'subtrans') {
		$cfoapal = coe_cfoapal_display_with_dashes($subtransaction['field_ipay_prdt_cfoapal_cfoapal']);
	}

	return $cfoapal;
}
