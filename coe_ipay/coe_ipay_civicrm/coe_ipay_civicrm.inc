<?php


function coe_ipay_civicrm_start($site_tag = NULL, $test = TRUE) { 
 
  $debug = variable_get('coe_ipay_debug_mode', FALSE);
  
  require_once(drupal_get_path('module','coe_ipay') .'/coe_ipay_drupal_functions.inc');
  require_once(drupal_get_path('module','coe_ipay') .'/coe_ipay.api.inc');

  if (!is_array($_SESSION['coe_ipay_civicrm'])) {
     return coe_ipay_response_show_error(array('ResponseCode' => COE_IPAY_FAILURE_LOCAL_ERROR), TRUE, 'end');
  }
  
  // only deal with 1 civicrm transaction at a time
  $transaction_ids = array_keys($_SESSION['coe_ipay_civicrm']);
  $transaction = $_SESSION['coe_ipay_civicrm'][$transaction_ids[0]];
  
  
  //unset($_SESSION['coe_ipay_civicrm'][$transaction_ids[0]]); 
 
  _coe_ipay_debug_show("coe_ipay_civicrm_start: transaction", $transaction, COE_IPAY_DEBUG_FUNCTION);
  
  // store any data in ipay trans and sub trans
 // print "<pre>"; print_r($transaction);
  $store_name = $transaction['processor']['name'];
  $store_handle = $transaction['processor'][COE_IPAY_CIVICRM_STORE_HANDLE_ARG];
  //  print 'store_handle'. $store_handle . 'store_name='. $store_name; die;
  _coe_ipay_debug_show("coe_ipay_civicrm_start: store_handle", $store_handle, COE_IPAY_DEBUG_FUNCTION);
  $store = coe_ipay_store_from_nid(coe_ipay_store_tag_to_nid($store_handle), TRUE);
  

  _coe_ipay_debug_show("coe_ipay_civicrm_start: store", $store, COE_IPAY_DEBUG_FUNCTION);
  
  $data = coe_ipay_context_data();
  $data['civicrm_trans'] = $transaction;
  _coe_ipay_debug_show("coe_ipay_context_data: data", $data, COE_IPAY_DEBUG_FUNCTION);
  
  $products = coe_ipay_civicrm_start_parse_civicrm($transaction, $store);
  _coe_ipay_debug_show("products derived", $products, COE_IPAY_DEBUG_FUNCTION);
  
  $response = coe_ipay_register($store, $products, $data, $test, $debug);
  _coe_ipay_debug_show("start response", $response, COE_IPAY_DEBUG_FUNCTION); 

  if ($response['ResponseCode'] == COE_IPAY_SUCCESS && $response['FullRedirectURL']) {
    drupal_goto($response['FullRedirectURL']);
  } else {
    coe_ipay_response_show_error($response, TRUE, 'end');
  }
  return "Receipt Page";
}


/** arrive here after ipay transaction complete and cck trans and subtrans populated
 *
 * need to simply redirect to civicrm to finish the transaction
 *
 PERHAPS THIS FUNCTION ISN'T NEEDED AT ALL?
 **/

function coe_ipay_civicrm_end() {
  require_once(drupal_get_path('module','coe_ipay') .'/coe_ipay_drupal_functions.inc');
  require_once(drupal_get_path('module','coe_ipay') .'/coe_ipay.api.inc');
  //dpm('coe_ipay_civicrm_end'); 
  $debug = variable_get('coe_ipay_debug_mode', FALSE);
  
  if (! ($token = check_plain($_GET['token'])) ) {
    drupal_set_message("Transaction Failed", "error");
  }
  
  $response = coe_ipay_finish($token, $test = TRUE, FALSE);
  _coe_ipay_debug_show("coe_ipay_finish response:", $response, COE_IPAY_DEBUG_FUNCTION);
   
  $trans_nid = $response['coe_ipay_trans_nid'];
  $trans_node = node_load($trans_nid);
  $data = unserialize($trans_node->field_ipay_trans_data[0]['value']);
  //dpm($data); dpm($response);
  if ($response['ResponseCode'] == COE_IPAY_SUCCESS) {
    
    drupal_goto($data['civicrm_trans']['returnURL']);
  } else {
    // not a success.  perhaps user cancelled, perhaps ipay errors
    $civicrm_data = array_shift($_SESSION['coe_ipay_civicrm']);
    $cancel_url = $civicrm_data['cancelURL'];
    $stage = "finish";
    $show_cancel_url = FALSE; // generally will redirect
    
    
    if (stripos($_SERVER['HTTP_REFERER'], "ipay/pc/public/actCCPaymentReturn.cfm") > 0 ) {
      $response['self_cancel'] = TRUE;
    } else {
      $response['self_cancel'] = FALSE;
    };

    if ($response['ResponseCode'] == COE_IPAY_FAILURE_AUTHORIZATION_FAILURE) {
      
    } else {

    }
    
    if ($cancel_url) {
      coe_ipay_response_show_error($response, $show_cancel_url, $stage);
      drupal_goto($cancel_url); // can't differentiate cancel from fail.
    } else {
      coe_ipay_response_show_error($response, TRUE, $stage);
    }

  }
    
  return "Transaction Error";

}



/**
 * needs to turn civicrm transaction into array of products and costs
 * from content_type_ipay_prod
 **/

function coe_ipay_civicrm_start_parse_civicrm(&$transaction, &$store) {
  // one product per transaction with ipay and civicrm
  //print "<pre>"; print_r($transaction); print_r($store); die;
  
  $products = array();
  
  // determine if event or what
  if ($transaction['params']['eventID']) {
    foreach($store['products'] as $product_nid => $product) {
      if ($product->field_ipay_prdt_fk_name[0]['value'] == "civicrm_event" &&
          $product->field_ipay_prdt_fk_value[0]['value'] == $transaction['params']['eventID']) {
        
        $products[$product_nid]['nid'] = $product_nid;  // this will give cfop, otherwise will default to site ccfoapal
        $products[$product_nid]['unit_price'] = $transaction['params']['amount'];
        $products[$product_nid]['qty'] = 1;
        $products[$product_nid]['cfoapal'] = $product->field_ipay_prdt_cfoapal[0]['cfoapal'];
         break; // 1 event product at a time
      }
    }
  }  

  return $products;
}