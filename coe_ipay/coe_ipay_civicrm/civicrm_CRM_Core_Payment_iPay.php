<?php 

/*
 +--------------------------------------------------------------------+
 | CiviCRM version 3.1                                                |
 +--------------------------------------------------------------------+
 | Copyright CiviCRM LLC (c) 2004-2009                                |
 +--------------------------------------------------------------------+
 | This file is a part of CiviCRM.                                    |
 |                                                                    |
 | CiviCRM is free software; you can copy, modify, and distribute it  |
 | under the terms of the GNU Affero General Public License           |
 | Version 3, 19 November 2007.                                       |
 |                                                                    |
 | CiviCRM is distributed in the hope that it will be useful, but     |
 | WITHOUT ANY WARRANTY; without even the implied warranty of         |
 | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.               |
 | See the GNU Affero General Public License for more details.        |
 |                                                                    |
 | You should have received a copy of the GNU Affero General Public   |
 | License along with this program; if not, contact CiviCRM LLC       |
 | at info[AT]civicrm[DOT]org. If you have questions about the        |
 | GNU Affero General Public License or the licensing of CiviCRM,     |
 | see the CiviCRM license FAQ at http://civicrm.org/licensing        |
 +--------------------------------------------------------------------+
*/

/**
 *
 * @package CRM
 * @copyright CiviCRM LLC (c) 2004-2009
 * $Id$
 *
 */

global $civicrm_root;
require_once($civicrm_root .'/CRM/Core/Payment.php');
require_once(drupal_get_path('module', 'coe_ipay') . '/coe_ipay.api.inc');
require_once('coe_ipay_civicrm.module');

class CRM_Core_Payment_ipay extends CRM_Core_Payment {
   
   const CHARSET  = 'iso-8859-1';
   
   const COE_IPAY_CIVICRM_STORE_HANDLE_ARG = 'user_name';
   const COE_IPAY_CIVICRM_STORE_HANDLE_LABEL ='iPay Store Handle (e.g. advancement)';
   const COE_IPAY_CIVICRM_START_PATH ='ipay/civicrm/start';
   const COE_IPAY_CIVICRM_END_PATH ='ipay/civicrm/end';

   static protected $_mode = null;
   static protected $_params = array();
   static private $_singleton = null; 
    
    /** 
     * Constructor 
     * 
     * @param string $mode the mode of operation: live or test
     *
     * @return void 
     */ 
    function __construct( $mode, &$paymentProcessor ) {
     // require_once('coe_ipay_civicrm.constants.inc');
     //print "<pre>"; print_r($paymentProcessor); die;
        $this->_mode             = $mode;
        $this->_paymentProcessor = $paymentProcessor;
        $this->_processorName    = ts('ipay');
        if ( ! $this->_paymentProcessor[self::COE_IPAY_CIVICRM_STORE_HANDLE_ARG] ) {
            CRM_Core_Error::fatal( ts( 'Could not find '. self::COE_IPAY_CIVICRM_STORE_HANDLE_LABEL .' for payment processor' ) );
        }   
    }
    /** 
     * singleton function used to manage this object 
     * 
     * @param string $mode the mode of operation: live or test
     *
     * @return object 
     * @static 
     * 
     */  
    static function &singleton( $mode, &$paymentProcessor ) {
        $processorName = $paymentProcessor['name'];
        if (self::$_singleton[$processorName] === null ) {
            self::$_singleton[$processorName] = new CRM_Core_Payment_ipay( $mode, $paymentProcessor );
        }
        return self::$_singleton[$processorName];
    }
    
    
    /** 
     * This function checks to see if we have the right config values 
     * 
     * @return string the error message if any 
     * @public 
     */ 
    function checkConfig( ) {
        $config = CRM_Core_Config::singleton( );
        
        
        $error = array( );
        if ( $this->_paymentProcessor['payment_processor_type'] == 'ipay' &&
            empty( $this->_paymentProcessor[self::COE_IPAY_CIVICRM_STORE_HANDLE_ARG] ) ) {
            $error[] = ts( self::COE_IPAY_CIVICRM_STORE_HANDLE_LABEL .' is not set in the Administer CiviCRM &raquo; Payment Processor.' );
        }
        
        if ( ! empty( $error ) ) {
            return implode( '<p>', $error );
        } else {
            return null;
        }
    }
    
    
    /**
     * express checkout code. modification of PayPal processor.
     * check PayPal documentation for more information
     * 
     * @param  array $params assoc array of input parameters for this transaction 
     * 
     * @return array the result in an nice formatted array (or an error object) 
     * @public
     */
    function setExpressCheckOut( &$params ) {
        
     //   print "<pre>ipay setExpressCheckOut: params"; print_r($params);
        $args = array( );

 
        $this->initialize( $args, 'SetExpressCheckout' );

        $args['paymentAction']  = $params['payment_action'];
        $args['amt']            = $params['amount'];
        $args['currencyCode']   = $params['currencyID'];
        $args['invnum']         = $params['invoiceID'];
        $args['returnURL'   ]   = $params['returnURL'];
        $args['cancelURL'   ]   = $params['cancelURL'];

        // Allow further manipulation of the arguments via custom hooks ..
        CRM_Utils_Hook::alterPaymentProcessorParams( $this, $params, $args );

        $result = $this->invokeAPI( $args );

        if ( is_a( $result, 'CRM_Core_Error' ) ) {  
            return $result;  
        }

        /* Success */
        return $result['token'];
    }

    /**
     * get details from paypal. Check PayPal documentation for more information
     *
     * @param  string $token the key associated with this transaction
     * 
     * @return array the result in an nice formatted array (or an error object) 
     * @public
     */
    function getExpressCheckoutDetails( $token ) {
      //  print "<pre>ipay getExpressCheckoutDetails: token"; print_r($token);
        $args = array( );

        $this->initialize( $args, 'GetExpressCheckoutDetails' );
        $args['token'] = $token;

        $result = $this->invokeAPI( $args );

        if ( is_a( $result, 'CRM_Core_Error' ) ) {  
            return $result;  
        }

        /* Success */
        $params                           = array( );
        $params['token']                  = $result['token'];
        $params['payer_id'    ]           = $result['payerid'];
        $params['payer_status']           = $result['payerstatus'];
        $params['first_name' ]            = $result['firstname'];
        $params['middle_name']            = $result['middlename'];
        $params['last_name'  ]            = $result['lastname'];
        $params['street_address']         = $result['shiptostreet'];
        $params['supplemental_address_1'] = $result['shiptostreet2'];
        $params['city']                   = $result['shiptocity'];
        $params['state_province']         = $result['shiptostate'];
        $params['postal_code']            = $result['shiptozip'];
        $params['country']                = $result['shiptocountrycode'];
        
        return $params;
    }

    /**
     * do the express checkout at paypal. Check PayPal documentation for more information
     *
     * @param  string $token the key associated with this transaction
     * 
     * @return array the result in an nice formatted array (or an error object) 
     * @public
     */
    function doExpressCheckout( &$params ) {
    //    print "<pre>ipay doExpressCheckout: params"; print_r($params);
        $args = array( );

        $this->initialize( $args, 'DoExpressCheckoutPayment' );

        $args['token']          = $params['token'];
        $args['paymentAction']  = $params['payment_action'];
        $args['amt']            = $params['amount'];
        $args['currencyCode']   = $params['currencyID'];
        $args['payerID']        = $params['payer_id'];
        $args['invnum']         = $params['invoiceID'];
        $args['returnURL'   ]   = $params['returnURL'];
        $args['cancelURL'   ]   = $params['cancelURL'];

        $result = $this->invokeAPI( $args );

        if ( is_a( $result, 'CRM_Core_Error' ) ) {  
            return $result;  
        }

        /* Success */
        $params['trxn_id']        = $result['transactionid'];
        $params['gross_amount'  ] = $result['amt'];
        $params['fee_amount'    ] = $result['feeamt'];
        $params['net_amount'    ] = $result['settleamt'];
        if ( $params['net_amount'] == 0 && $params['fee_amount'] != 0 ) {
            $params['net_amount'] = $params['gross_amount'] - $params['fee_amount'];
        }
        //$params['payment_status'] = $result['paymentstatus'];
        $params['payment_status'] = '$'. $params['amount'] . " has been charged to your credit card. It will appear on your statement as U of IL Online Payment";
        $params['pending_reason'] = $result['pendingreason'];
        
        return $params;
    }

    function initialize( &$args, $method ) {
        $args['user'     ] = $this->_paymentProcessor[self::COE_IPAY_CIVICRM_STORE_HANDLE_ARG];
        $args['version'  ] = 3.3;
        $args['method'   ] = $method;
    }

    /**
     * this is a pass through, because sometimes civicrm doesn't know
     * the appropriate payment function for a payment processor type.
     */
    function doDirectPayment( &$params, $component = 'contribute' ) {
         $backtrace = debug_backtrace();

$callPath = $backtrace[1]['function'];
$callPath .= ' [Line ' . $backtrace[1]['line'];
$callPath .= '; File: ' . $backtrace[1]['file'] . ']';


//print $callPath;
//print "<pre>";
//print_r($backtrace); die;
       //  doTransferCheckout($params, $component);
        // transfer checkout should redirect, so params are meaningless below.
           
        return $params;
    }



    function doTransferCheckout( &$params, $component = 'contribute' ) {
       // print "<pre>ipay doTransferCheckout: params"; print_r($params); die;
        $config =& CRM_Core_Config::singleton( );

        if ( $component != 'contribute' && $component != 'event' ) {
            CRM_Core_Error::fatal( ts( 'Component is invalid' ) );
        }
          
        $ipay_store_handle = $this->_paymentProcessor[self::COE_IPAY_CIVICRM_STORE_HANDLE_ARG];
//dpm($ipay_store_handle);
        $url    = ( $component == 'event' ) ? 'civicrm/event/register' : 'civicrm/contribute/transact';
        $cancel = ( $component == 'event' ) ? '_qf_Register_display'   : '_qf_Main_display';
        $returnURL = CRM_Utils_System::url( $url,
                                            "_qf_ThankYou_display=1&qfKey={$params['qfKey']}",
                                            true, null, false );
        $cancelURL = CRM_Utils_System::url( $url,
                                            "$cancel=1&cancel=1&qfKey={$params['qfKey']}",
                                            true, null, false );
        
        //@todo.  what should end of url be? maybe nothing since data is stored in session.
        $drupaliPayURL =  CRM_Utils_System::url(self::COE_IPAY_CIVICRM_START_PATH .'/'. $ipay_store_handle , null, true,
                 $fragment = null, $htmlize = false,
                 $frontend = false );
        
        $trans_key = $params['qfKey'];
        $_SESSION['coe_ipay_civicrm'][$trans_key]['processor'] = $this->_paymentProcessor;
        $_SESSION['coe_ipay_civicrm'][$trans_key]['returnURL'] = $returnURL;
        $_SESSION['coe_ipay_civicrm'][$trans_key]['cancelURL'] = $cancelURL;
        $_SESSION['coe_ipay_civicrm'][$trans_key]['params'] = $params;
        
     //  print "<pre>$drupaliPayURL";
      // print_r($_SESSION['coe_ipay_civicrm'][$trans_key]); //die;
        
        // this redirect will move user to drupal endpoint for cyberbash
        // when finished will return here

        
        CRM_Utils_System::redirect( $drupaliPayURL );
    }

    /**
     * hash_call: Function to perform the API call to PayPal using API signature
     * @methodName is name of API  method.
     * @nvpStr is nvp string.
     * returns an associtive array containing the response from the server.
     */
    function invokeAPI( $args, $url = null ) {

        if ( $url === null ) {
            if ( empty( $this->_paymentProcessor['url_api'] ) ) {
                CRM_Core_Error::fatal( ts( 'Please set the API URL. Please refer to the documentation for more details' ) );
            }

            $url = $this->_paymentProcessor['url_api'] . 'nvp';
        }

        if ( !function_exists('curl_init') ) {
            CRM_Core_Error::fatal("curl functions NOT available.");
        }

        //setting the curl parameters.
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_VERBOSE, 1);

        //turning off the server and peer verification(TrustManager Concept).
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);

        $p = array( );
        foreach ( $args as $n => $v ) {
            $p[] = "$n=" . urlencode( $v );
        }

        //NVPRequest for submitting to server
        $nvpreq = implode( '&', $p );

        //setting the nvpreq as POST FIELD to curl
        curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);

        //getting response from server
        $response = curl_exec( $ch );

        //converting NVPResponse to an Associative Array
        $result = self::deformat( $response );

        if ( curl_errno( $ch ) ) {
            $e =& CRM_Core_Error::singleton( );
            $e->push( curl_errno( $ch ),
                      0, null,
                      curl_error( $ch ) );
            return $e;
        } else {
			curl_close($ch);
        }

        if ( strtolower( $result['ack'] ) != 'success' &&
             strtolower( $result['ack'] ) != 'successwithwarning' ) {
            $e =& CRM_Core_Error::singleton( );
            $e->push( $result['l_errorcode0'],
                      0, null,
                      "{$result['l_shortmessage0']} {$result['l_longmessage0']}" );
            return $e;
        }

        return $result;
    }

    /** This function will take NVPString and convert it to an Associative Array and it will decode the response.
     * It is usefull to search for a particular key and displaying arrays.
     * @nvpstr is NVPString.
     * @nvpArray is Associative Array.
     */

    static function deformat( $str )
    {
        $result = array();

        while ( strlen( $str ) ) {
            // postion of key
            $keyPos = strpos( $str, '=' );

            // position of value
            $valPos = strpos( $str, '&' ) ? strpos( $str, '&' ): strlen( $str );

            /*getting the Key and Value values and storing in a Associative Array*/
            $key = substr( $str, 0, $keyPos );
            $val = substr( $str, $keyPos + 1, $valPos - $keyPos - 1 );

            //decoding the respose
            $result[ strtolower( urldecode( $key ) ) ] = urldecode( $val );
            $str = substr( $str, $valPos + 1, strlen( $str ) );
        }

        return $result;
    }

}



class CRM_Event_Payment_ipay extends CRM_Core_Payment_ipay {
    /** 
     * We only need one instance of this object. So we use the singleton 
     * pattern and cache the instance in this variable 
     * 
     * @var object 
     * @static 
     */ 
    static private $_singleton = null; 
    
    /** 
     * Constructor 
     * 
     * @param string $mode the mode of operation: live or test
     *
     * @return void 
     */ 
    function __construct( $mode, &$paymentProcessor ) {
        parent::__construct( $mode, $paymentProcessor );
    }

    /** 
     * singleton function used to manage this object 
     * 
     * @param string $mode the mode of operation: live or test
     * 
     * @return object 
     * @static 
     * 
     */ 
    static function &singleton( $mode, &$paymentProcessor ) {
        if (self::$_singleton === null ) { 
            self::$_singleton =& new CRM_Event_Payment_ipay( $mode, $paymentProcessor );
        } 
        return self::$_singleton; 
    } 

    function doTransferCheckout( &$params ) {
        parent::doTransferCheckout( $params, 'event' );
    }

}


