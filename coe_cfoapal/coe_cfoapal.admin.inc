<?php

/**
 *   @file
 *   
 *   admin/configuration functions for cfoapal field.  
 *   
 **/



/**
 *   configure settings for a single coe_cfoapal field.
 **/

function coe_cfoapal_field_settings_form($field) {
  $form = array(
    '#element_validate' => array('coe_cfoapal_field_settings_validate'),
  );

  $form['coe_cfoapal_required'] = array(
    '#type' => 'fieldset',
    '#title' => t('CFOAPAL Parts Required'),
    '#weight' => 5,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    );
  
 
  foreach (_coe_cfoapal_subfields() as $field_stem => $conf) {
  //  dpm($conf);
    $key = 'require_'. $field_stem;
    $form['coe_cfoapal_required'][$key] = array(
    '#type' => 'checkbox',
    '#required' => FALSE,
    '#title' => t('Require '. $conf['#title']),
    '#options' => array(1,0),
    '#default_value' => isset($field[$key]) ? $field[$key] : $conf['#required'],
    );   
  }
  
 $form['coe_cfoapal_visible'] = array(
    '#type' => 'fieldset',
    '#title' => t('CFOAPAL Parts Visible'),
    '#weight' => 5,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    );
  
 
  foreach (_coe_cfoapal_subfields() as $field_stem => $conf) {
  //  dpm($conf);
    $key = 'show_'. $field_stem;
    $form['coe_cfoapal_visible'][$key] = array(
    '#type' => 'checkbox',
    '#required' => FALSE,
    '#title' => t('Show '. $conf['#title']),
    '#options' => array(1,0),
    '#default_value' => isset($field[$key]) ? $field[$key] : $conf['#shown'],
    );   
  }
  
  cache_clear_all('coe_cfoapal__','cache', $wildcard = TRUE);  // clear cache of coe_cfoapal config for this node type
  return $form;
}

function _coe_cfoapal_field_settings($op, $field) {
  switch ($op) {
    case 'form':
      return coe_cfoapal_field_settings_form($field);
    break; //I break for clarity after returns

    case 'validate':
      coe_cfoapal_field_settings_validate($form, $form_state);
    break;

    case 'save':
       foreach(_coe_cfoapal_subfields() as $stem => $conf) {
         $options[] = 'require_'. $stem;
         $options[] = 'show_'. $stem;
       }
      return $options;
    break; // I break for clarity after returns

    case 'database columns':
      return array(
        'cfoapal' => array('type' => 'varchar', 'length' => 43, 'not null' => FALSE, 'sortable' => TRUE),
         );
    break; // I break for clarity after returns
  

    case 'views data':

  }
}

function _coe_cfoapal_widget_info() {
  return array(
    'coe_cfoapal_vertical_fields' => array(
      'label' => 'Separate Vertical Inputs',
      'field types' => array('coe_cfoapal'),
      'multiple values' => CONTENT_HANDLE_CORE,
      'callbacks' => array('default value' => CONTENT_CALLBACK_CUSTOM ),
    ),
    'coe_cfoapal_horizontal_fields' => array(
      'label' => 'Separate Horizontal Inputs',
      'field types' => array('coe_cfoapal'),
      'multiple values' => CONTENT_HANDLE_CORE,
      'callbacks' => array('default value' => CONTENT_CALLBACK_CUSTOM ),
    ),
    'coe_cfoapal_single_field' => array(
      'label' => 'Single Hyphenated Input',
      'field types' => array('coe_cfoapal'),
      'multiple values' => CONTENT_HANDLE_CORE,
      'callbacks' => array('default value' => CONTENT_CALLBACK_CUSTOM ),
    ),
  );
}



