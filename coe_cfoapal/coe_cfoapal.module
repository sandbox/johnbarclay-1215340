<?php
// $Id: coe_cfoapal.module,v 1.24.2.35 2009/04/20 04:09:18  Exp $

/**
 * @file
 * Defines simple coe_cfoapal field types for content types
 *
 * http://www.lullabot.com/articles/creating-custom-cck-fields
 *   great article on creating a simple field.  Doesn't address
 *   compound fields
 *   
 * http://poplarware.com/cckfieldmodule.html
 *    example of creating a compound field
 *    
 */


/**
 * Implementation of hook_field_info().
 */
function coe_cfoapal_field_info() {
  return array(
    'coe_cfoapal' => array(
      'label' => t('CFOAPAL'),
      'description' => t('Field for entering University of Illinois CFOAPALs.'),
    ),
  );
}

/**
 * Implementation of hook_elements(). http://api.drupal.org/api/function/hook_elements/6
 *
 * where hook is the field "coe_cfoapal"
 * and the array keys are the widgets the field supports
 * 
 * hook is not the widget name "coe_cfoapal_widget"
 * http://drupal.org/node/169815 for process outline
 */

function coe_cfoapal_elements() {
  
  $elements['coe_cfoapal_vertical_fields'] =  array(
      '#input' => TRUE,
      '#columns' =>  array('cfoapal'),
      '#delta' => 0,
      '#process' => array('coe_cfoapal_vertical_fields_process'),
      '#element_validate' => array('coe_cfoapal_element_validate'),
    );
  
  //  need to add elements for other types (horizontal and single field)
    return $elements;
}



/**
 * Implementation of hook_content_is_empty().
 *
 * form api needs to know when this field is considered empty
 * so required fields can be validated.
 */
function coe_cfoapal_content_is_empty($item, $field) {
 // dpm($item); //dpm($field);
  if (! is_array($item)) {
    return TRUE;
  }
  switch ($field['widget']['type']) {
    
    case 'coe_cfoapal_vertical_fields':
     // dpm(array_values($item));
      return is_null(trim(join("",array_values($item))));
    break;
  }
}

/**
 * Implementation of hook_field_settings().  See http://drupal.org/node/354365
 *
 * this is used to configure the field when a field is being added to a content type
 *   and when modifying the field conf afterward.
 */
function coe_cfoapal_field_settings($op, $field) {
  require_once('coe_cfoapal.admin.inc');
  return _coe_cfoapal_field_settings($op, $field);
}

/**
 * Implementation of hook_widget(), where hook is the field name, not the widget name
 */
function coe_cfoapal_widget(&$form, &$form_state, $field, $items, $delta = 0) {
  $element = array(
    '#type' => $field['widget']['type'],
    '#default_value' => isset($items[$delta]) ? $items[$delta] : '',
  );
  return $element;

}



/**
 * Implementation of hook_field().
 */
function coe_cfoapal_field($op, &$node, $field, &$items, $teaser, $page) {
   
  switch ($op) {
    case 'load':
     
    break;

    case 'validate':
      $optional_field_found = FALSE;
    break;

    case 'insert':
    case 'update':

      foreach ($items as $delta => $item) {
        $value = join('-',$item);
        $items[$delta]['cfoapal'] = $value;
      }
    break;

    case 'presave':
      break;

    case 'sanitize':
      break;
  }
}




/**
 * Implementation of hook_widget_info(), where hook is the field, not the widget name
 */
function coe_cfoapal_widget_info() {
  require_once('coe_cfoapal.admin.inc');
  return _coe_cfoapal_widget_info();
}






function coe_cfoapal_element_validate(&$element,&$form_state) {
 $values = $element['#value'];
 $subfields = _coe_cfoapal_subfields();
 
  // don't validate if empty; required field validation will take care of that.
  if (coe_cfoapal_content_is_empty($value, array('widget' => array ('type' => 'coe_cfoapal_vertical_fields')))) {
   return;
  }
 
 // required fields
  foreach ($subfields as $field_stem => $conf) {
    if ($field['require_'. $field_stem] && !$values[$field_stem]) {
       form_error($element[$field_stem], t('%name is required.', array('%name' => $conf['#title'])));
    }
   }
   
  // six digit integer fields
  foreach (array('fund','org','account','program') as $field_stem) {
    $conf = $subfields[$field_stem];
    $value = $values[$field_stem];
     if ($value !== '' && (!is_numeric($value) || intval($value) != $value)) {
       form_error($element[$field_stem], t('%name must contain only numbers.', array('%name' => $conf['#title'])));
     }
     elseif ($value !== '' && strlen($value) != 6) {
       form_error($element[$field_stem], t('%name must be six digits.', array('%name' => $conf['#title'])));
     }
   }
   

  
 
}

/**
 * Process the coe_cfoapal type element before displaying the field.
 *
 * Build the form element. When creating a form using FAPI #process,
 * note that $element['#value'] is already set.
 *
 * The $fields array is in $form['#field_info'][$element['#field_name']].
 */
function coe_cfoapal_vertical_fields_process(&$element, $edit, &$form_state, &$form) {
  drupal_add_css(drupal_get_path('module', 'coe_cfoapal') .'/coe_cfoapal.css', 'module', 'all', FALSE);
  $field = $form['#field_info'][$element['#field_name']];
  $cfoapal_value = $element['#value']['cfoapal'];
  $cfoapal_parts = explode('-', $cfoapal_value);
  //dpm('coe_cfoapal_vertical_fields_process'); dpm($field); dpm('element'); dpm($element); dpm($cfoapal_parts);
  $place = 0;
  foreach (_coe_cfoapal_subfields() as $field_stem => $conf) {
   if ($field['show_'. $field_stem] ) {
     $element[$field_stem] = $conf;
     $element[$field_stem]['#required'] = $field['require_'. $field_stem];
     $element[$field_stem]['#default_value'] =  $cfoapal_parts[$place];
     $element[$field_stem]['#attributes']['class'] .= ' text';
     $place ++;
   }
  }
  return $element;
}

// @todo need to make this overrideable/themeable 
function theme_coe_cfoapal_vertical_fields(&$element) {
  //return theme( 'form_element', $element, $element['#children'] );
  return _theme_coe_cfoapal_vertical_fields($element, $element['#children']);
}

/**
 * format as form fields
 **/
function _theme_coe_cfoapal_vertical_fields(&$element, $value) {

$type =  $element['#type'];
$title = $element['#title'];
$required = !empty($element['#required']) ? '<span class="form-required" title="'. t('This field is required.') .'">*</span>' : '';
$description = $element['#description'];

$output = <<<EOF
<fieldset class='form-items collapsible $type'>
  <legend>$title</legend>
  <div class="description">
    $required
    $description
  </div>
  <div class='coe_cfoapal-codes-group'>
    $value
  </div>
</fieldset>
EOF;

return $output;

}


/**
 * Implementation of hook_field_formatter_info().
 */
function coe_cfoapal_field_formatter_info() {
  return array(
    'default' =>
    array(
          'label' => t('Title, as coe_cfoapal (default)'),
          'field types' => array('coe_cfoapal'),
          'multiple values' => CONTENT_HANDLE_CORE),
  );
}


/**
 * Theme function for 'plain' text field formatter.
 * should be of the form N-NNNNNN-NNNNNN-N
 */
function theme_coe_cfoapal_formatter_default($element, $show_units = TRUE) {
  // already formatted nicely as stored in db.
  return coe_cfoapal_display_with_dashes($element['#item']['cfoapal'],'-');

}

function coe_cfoapal_display_with_dashes($cfoapal_in_db) {
  return trim($cfoapal_in_db,'-');
}

function coe_cfoapal_theme() {
  return array(
    'coe_cfoapal_vertical_fields' => array('arguments' => array('element' => NULL)  ),
    'coe_cfoapal_formatter_default' => array('arguments' => array('element' => NULL)  ),
  );
}

/**
 * Custom field settings manipulation.
 *
 * CCK field settings can't use form_set_value(),
 * so do it in a custom function.
 */
function coe_cfoapal_field_settings_validate(&$form, &$form_state) {

}




/**
 * used for configuration of cfoapal codes
 **/
 
function _coe_cfoapal_subfields() {
  
 // static $subfields;
  
 // if (is_array($subfields)) { return $subfields; }
  
  $subfields = array(
    'chart' => array(
      '#title' => 'Chart Code',
      '#required' => 1,
      '#description' => NULL,
      '#type' => 'select',
      '#attributes' => array('size' => 1),
      '#options' => array(
         NULL => 'select chart',
         '1' => '1 = Urbana',
         '2' => '2 = Chicago',
         '4' => '4 = Springfield',
         '9' => '9 = Univ. Admin',
       )
    ),
    'fund' => array(
      '#title' => 'Fund Code',
      '#required' => 1,
      '#maxlength' => 6,
      '#description' => '6 Digits.',
      '#type' => 'textfield',
      '#size' => 8,
      ),
    'org' => array(
      '#title' => 'Organization Code',
      '#required' => 1,
      '#description' => '6 Digits.',
      '#maxlength' => 6,
      '#type' => 'textfield',
      '#size' => 8,
      ),
    'account' => array(
      '#title' => 'Account Code',
      '#required' => 1,
      '#description' => '6 Digits.',
      '#maxlength' => 6,
      '#type' => 'textfield',
      '#size' => 8,
      ),
    'program' => array(
      '#title' => 'Program Code',
      '#required' => 1,
      '#maxlength' => 6,
      '#description' => '6 Digits.',
       '#type' => 'textfield',
       '#size' => 8,
      ),
    'activity' => array(
      '#title' => 'Activity Code',
      '#required' => 0,
      
      '#description' => '6 Digits.',
       '#type' => 'textfield',
       '#size' => 8,
      '#maxlength' => 6,
      ),
    'location' => array(
      '#title' => 'Location Code',
      '#required' => 0,
      '#description' => '6 Digits.',
      '#maxlength' => 6,
      '#type' => 'textfield',
      '#size' => 8,
      ),
      );
  
  return $subfields;
}


