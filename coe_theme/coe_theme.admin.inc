<?php

/**
 * note that these constants are generally only available in this form; not the entire modue.
 * on install they are
 * propagated to the drupal variables table by the hook_install function for this module
 **/

define('COE_THEME_ACCESS_DENIED_MESSAGE', '<div class="messages warning">You must logon to see
    this page.  Please logon or create an account</div>');
define('COE_THEME_USERNAME_LABEL', 'Enter your UIUC Netid or COE Apps Account Username');
define('COE_THEME_PASSWORD_LABEL', 'Enter your Active Directory Password or COE Apps Password');
    
function coe_theme_configure(&$form_state) {
 $form['#title'] = "Configure Setttings and Constants related to theme layer, such as access denied message.";
 $form['#prefix'] = "The values set here are generally used in the theming layer with a variable_get() call."; 
 _coe_theme_admin_form($form);
  return system_settings_form($form);
}

function _coe_theme_admin_form(&$form) {
 
   $form['coe_theme_configure']['coe_theme_access_denied_message'] = array(
    '#type' => 'textarea',
    '#size' => 50,
    '#required' => FALSE,
    '#title' => t('Text to show in access denied message'),
    '#description' => "eg ". COE_THEME_ACCESS_DENIED_MESSAGE,
    '#default_value' => variable_get('coe_theme_access_denied_message', COE_THEME_ACCESS_DENIED_MESSAGE), 
    );
 
   $form['coe_theme_configure']['coe_theme_username_label'] = array(
    '#type' => 'textfield',
    '#size' => 70,
    '#required' => FALSE,
    '#title' => t('Label for logon username field'),
    '#description' => "eg ". COE_THEME_USERNAME_LABEL,
    '#default_value' => variable_get('coe_theme_username_label', COE_THEME_USERNAME_LABEL), 
    );
   
   $form['coe_theme_configure']['coe_theme_password_label'] = array(
    '#type' => 'textfield',
    '#size' => 70,
    '#required' => FALSE,
    '#title' => t('Label for password field'),
    '#description' => "eg ". COE_THEME_PASSWORD_LABEL,
    '#default_value' => variable_get('coe_theme_password_label', COE_THEME_PASSWORD_LABEL), 
    );
   
}

